package ru.rbt.integration.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayDeque;
import java.util.StringJoiner;

@Controller
public class AdministrateController {
  public final String firstPage = "index";

  @Autowired
  EntityManagerFactory emf;

  private ArrayDeque<String> states = new ArrayDeque<String>(10);

  @RequestMapping(value = "/"+firstPage, method = RequestMethod.GET)
  public String index(Model model) {
    update(model);
    return firstPage;
  }

  @RequestMapping(value = "/test", method = RequestMethod.GET)
  public String test() {
    return "Im working right now";
  }

  private Model update(Model model){
    StringJoiner joiner = new StringJoiner("<br>");
    states.stream().forEach(state -> joiner.add(state));
    model.addAttribute("status", joiner.toString());
    return model;
  }
}
