package ru.rbt.integration.service.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.rbt.integration.service.helper.SystemHelper;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;

@RestController
@RequestMapping("/karaf")
public class KarafProjectService {
  @Autowired
  EntityManagerFactory emf;

  @Value("${ssh.info.script}")
  private String infoScript = "cd /var/lib/rbt-integration/apache-karaf-2.3.10/bin && sshpass -pfarakrbt218 ssh karaf-service \"admin:list\"";
  @Autowired
  SystemHelper system;
  private boolean isRestart = false;
  protected Logger logger = LoggerFactory.getLogger(KarafProjectService.class);
  private HashMap<String,String> serviceToRestartMap =new HashMap<>();
  {
      serviceToRestartMap.put("logic", "cd ~/apache-karaf-2.3.10/bin/adminCommands/ && ./restartLogicClean.sh");
      serviceToRestartMap.put("accounting", "cd ~/apache-karaf-2.3.10/bin/adminCommands/ && ./restartAccountingClean.sh");
      serviceToRestartMap.put("test", "pwd");
  }
  @RequestMapping(value = "/restart")
  public synchronized ResponseEntity restart(@RequestParam("service") String service) {
    logger.info("Попытка перезапуска сервиса " + service); //todo отслеживать пользователя
    String result = "";
    if(isRestart){
      return new ResponseEntity(result, HttpStatus.OK);
    }
    isRestart = true;
    try {
        if(serviceToRestartMap.containsKey(service)) {
          result = system.run(serviceToRestartMap.get(service)).replace("\n", "<br>");
        }
    } catch (Exception ignored) {
      System.out.println(ignored.getMessage());
    } finally {
      isRestart = false;
    }
    return new ResponseEntity(result, HttpStatus.OK);
  }

  @RequestMapping(value = "/info")
  public synchronized ResponseEntity info() {
    String result = "";
    try {
        result = system.run(infoScript);
        result = result.replace("\n", "<br>");
    } catch (Exception ignored) {
      result = ignored.getMessage();
      System.out.println(ignored.getMessage());
    } finally {
    }
    return new ResponseEntity(result, HttpStatus.OK);
  }
}
