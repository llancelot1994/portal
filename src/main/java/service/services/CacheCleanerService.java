package ru.rbt.integration.service.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rbt.integration.service.helper.Command;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cache")
public class CacheCleanerService {
    protected Logger logger = LoggerFactory.getLogger(CacheCleanerService.class);

    public String getCommandAndListParameters(){
        return "";
    }

    @Autowired
    private Command command;
    private boolean isBusy = false;

    @RequestMapping(value = "/run")
    public String run(String postfix){
        if(command.getPostfix().containsKey(postfix)){}
        return null;
    }
    @RequestMapping(value = "/runById")
    public String run(int cache_id){
        if(isBusy)
            return "Кто-то уже все сбросил.";
        isBusy = true;
        logger.info("Запущен сброс кэша");
        String result = "";
        try {
            if (command.getPostfix().values().contains(cache_id)) {
                result = command.run(String.valueOf(cache_id)).entrySet().stream().map(value -> value.getKey() + " " + value.getValue().toLowerCase().contains("done") + "<br/>").collect(Collectors.joining());
                lastParamCallDate.put(cache_id, new Date());
            }
            isBusy = false;
            logger.info("Завершен сброс кэша");
        } catch (Exception e){
            logger.info("Сброс кэша не удался");
        }
        return result;
    }

    @RequestMapping(value = "/list")
    public String list(){
        HashMap<String, String> result = new HashMap<>();
        command.getPostfix().keySet().stream().parallel().forEach(
            name -> {
                if (lastParamCallDate.containsKey(command.getPostfix().get(name)))
                    result.put(name, String.valueOf(lastParamCallDate.get(command.getPostfix().get(name))));
                else
                    result.put(name, "не определенно");
            });
        return result.entrySet().stream().map(x-> "<tr><td>"+x.getKey() +"</td><td>" + x.getValue() + "</td></tr>").collect(Collectors.joining());
    }
    HashMap<Integer, Date> lastParamCallDate = new HashMap<>();
}
