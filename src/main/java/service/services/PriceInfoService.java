package ru.rbt.integration.service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.rbt.integration.core.helper.DbUtils;
import ru.rbt.integration.service.entity.PriceResult;
import ru.rbt.integration.service.entity.RemainsResult;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import javax.ws.rs.PathParam;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/price")

public class PriceInfoService {

    @Autowired
    EntityManagerFactory emf;
    @RequestMapping(value = "/info")
    public synchronized ResponseEntity info() {
        EntityManager em = emf.createEntityManager();
        List<PriceResult> result = new ArrayList<>();
        try {
            result = em.createNamedQuery("PriceResult.getPriceLoadResult", PriceResult.class)
                .getResultList();
        } catch (Exception ignored) {
            System.out.println(ignored.getMessage());
        } finally {
            DbUtils.closeEntityManager(em);
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/find")
    public synchronized ResponseEntity find(@RequestParam("shopId") String shopId) {
        EntityManager em = emf.createEntityManager();
        List<PriceResult> result = new ArrayList<>();
        try {
            result = em.createNamedQuery("PriceResult.getPriceLoadResultFromMask", PriceResult.class)
                .setParameter(1, shopId)
                .getResultList();
        } catch (Exception ignored) {
            System.out.println(ignored.getMessage());
        } finally {
            DbUtils.closeEntityManager(em);
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }


    @RequestMapping(value = "/reloadOnRemain")
    public synchronized ResponseEntity reloadOnRemain(@PathParam("shopId") int shopId) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createNamedQuery("PriceResult.reloadOnRemain").setParameter(1, shopId).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception ignored) {
            em.getTransaction().rollback();
            System.out.println(ignored.getMessage());
        } finally {
            DbUtils.closeEntityManager(em);
        }
        return new ResponseEntity("Готово", HttpStatus.OK);
    }

    @RequestMapping(value = "/reload")
    public synchronized ResponseEntity reload(@PathParam("shopId") int shopId) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createNamedQuery("PriceResult.reload").setParameter(1, shopId).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception ignored) {
            em.getTransaction().rollback();
            System.out.println(ignored.getMessage());
        } finally {
            DbUtils.closeEntityManager(em);
        }
        return new ResponseEntity("Готово", HttpStatus.OK);
    }
    @RequestMapping(value = "/reloadOnError")
    public synchronized ResponseEntity reloadOnError(@PathParam("shopId") int shopId) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createNamedQuery("PriceResult.reloadOnError").setParameter(1, shopId).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception ignored) {
            em.getTransaction().rollback();
            System.out.println(ignored.getMessage());
        } finally {
            DbUtils.closeEntityManager(em);
        }
        return new ResponseEntity("Готово", HttpStatus.OK);
    }
}
