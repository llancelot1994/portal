package ru.rbt.integration.service.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rbt.integration.core.helper.DbUtils;
import ru.rbt.integration.service.entity.WaitOrders;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.PathParam;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrdersService {

    @Autowired
    private EntityManagerFactory sessionFactory;
    @RequestMapping(value = "/waitInfo")
    public synchronized ResponseEntity getWaiting() {
        EntityManager em = sessionFactory.createEntityManager();
        List<WaitOrders> result = new ArrayList<>();
        try {
            result = em.createNamedQuery("WaitOrders.getWaitOrders", WaitOrders.class)
                .getResultList();
        } catch (Exception ignored) {
            System.out.println(ignored.getMessage());
        } finally {
            DbUtils.closeEntityManager(em);
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/resend")
    public synchronized ResponseEntity resendOrder(@PathParam("orderId") long orderId) {
        EntityManager em = sessionFactory.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createNativeQuery(""+
              "update web.orders_retry_wait "+
              "  set waiting = 0            "+
              "  where   orders_id = ?      ").setParameter(1, orderId).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception ignored) {
            System.out.println(ignored.getMessage());
            em.getTransaction().rollback();
        } finally {
            DbUtils.closeEntityManager(em);
        }
        return new ResponseEntity("Готово", HttpStatus.OK);
    }

    @RequestMapping(value = "/resendAll")
    public synchronized ResponseEntity resendOrders() {
        EntityManager em = sessionFactory.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createNativeQuery(""+
                "update web.orders_retry_wait                   "+
                "  set waiting = 0                              "+
                "  where orders_id >0 and date_add>date(now())  ").executeUpdate();
            em.getTransaction().commit();
        } catch (Exception ignored) {
            System.out.println(ignored.getMessage());
            em.getTransaction().rollback();
        } finally {
            DbUtils.closeEntityManager(em);
        }
        return new ResponseEntity("Готово", HttpStatus.OK);
    }
}
