package ru.rbt.integration.service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.rbt.integration.core.helper.DbUtils;
import ru.rbt.integration.service.entity.RemainsResult;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/remain")
public class RemainInfoService {

    @Autowired
    EntityManagerFactory emf;

    @RequestMapping(value = "/info")
    public ResponseEntity info(@RequestParam("shop") String shop, @RequestParam("item") String item) {
        EntityManager em = emf.createEntityManager();
        List<RemainsResult> result = new ArrayList<>();
        try {
            result = em.createNamedQuery("RemainsResult.getByShopAndItem", RemainsResult.class)
                .setParameter("item", item)
                .setParameter("shop", shop)
                .getResultList();
        } catch (Exception ignored) {
            System.out.println(ignored.getMessage());
        } finally {
            DbUtils.closeEntityManager(em);
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }
}
