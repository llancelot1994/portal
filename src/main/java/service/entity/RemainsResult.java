package ru.rbt.integration.service.entity;

import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
@NamedNativeQuery(
    name ="RemainsResult.getByShopAndItem",
    query = "" +
        "  select                                                                                           " +
        "    distinct                                                                                       " +
        "       i.name                    as item                                                           " +
        "     , i.onec_id                 as item_onec_id                                                   " +
        "     , i.item_id                 as item_id                                                        " +
        "     , r.name                    as region                                                         " +
        "     ,r.region_id                as region_id                                                      " +
        "     ,iss.delivery_terminal_onec as delivery_terminal_onec                                         " +
        "     ,s.pickup_available         as pickup_of_terminal                                             " +
        "     ,s.onec_id                  as stock                                                          " +
        "     ,w.delivery_days            as delivery_day                                                   " +
        "     ,rrsf.rule_uid              as rule_uid                                                       " +
        "     ,rrsf.time_from             as time_from                                                      " +
        "     ,rrsf.time_to               as time_to                                                        " +
        ", CASE rrsf.sell_terr_formation                                                                    " +
        "              WHEN -1 THEN -1                                                                      " +
        "              WHEN 1 THEN concat('1 \\\"', sspl.tf_name, '\\\" -> \\\"', ssplto.tf_name, '\\\"')   " +
        "              WHEN 0 THEN concat('0 \\\"', sspl.tf_name, '\\\" -> \\\"', ssplto.tf_name, '\\\"')   " +
        "                END as sell_terr_formation                                                         " +
        "        , CASE rrsf.delivery_terminal                                                              " +
        "              WHEN -1 then -1                                                                      " +
        "              WHEN 1 THEN 1                                                                        " +
        "              WHEN 0 THEN 0                                                                        " +
        "                END as delivery_terminal                                                           " +
        "        , CASE rrsf.axapta_storage                                                                 " +
        "              WHEN -1 THEN -1                                                                      " +
        "              WHEN 1 THEN 1                                                                        " +
        "              WHEN 0 THEN 0                                                                        " +
        "                END as axapta_storage                                                              " +
        "        ,rrsf.delivery_days_after       as delivery_days_after                                     " +
        "        ,dr.perm_point_Iss_some_region  as this_region                                             " +
        "        ,dr.perm_point_Iss_other_region as other_region                                            " +
        "        ,dr.perm_point_Iss_centr_stock  as centr_stock                                             " +
        ", sto.onec_id                                                                                      " +
        "from web.item_cache_remain icr                                                                     " +
        "         JOIN common.region r ON icr.region_id = r.region_id and r.status                          " +
        "         join web.item i on icr.item_id = i.item_id                                                " +
        "         join web.shop s on icr.shop_id_from = s.shop_id                                           " +
        "         join rbt_integration.stores_properties_loaded sspl on s.onec_id = sspl.shop_onec_id       " +
        "         join web.shop sto on sto.region_id = icr.region_id and sto.is_main                        " +
        "         join rbt_integration.stores_properties_loaded ssplto on sto.onec_id = ssplto.shop_onec_id " +
        "         join rbt_integration.internet_shop_store_loaded iss on iss.shop_onec_id_to = sto.onec_id  " +
        "         left join rbt_integration.warehouse_loaded w                                              " +
        "             on w.shop_onec_id_to = iss.delivery_terminal_onec and w.shop_onec_id_from = s.onec_id " +
        "         left join rbt_integration.delivery_rule_to_shop as dr on dr.shop_id = sto.shop_id         " +
        "         join rbt_integration.v_rules_region_shop_from rrsf                                        " +
        "         on rrsf.region_id = r.region_id and rrsf.onec_id_from = s.onec_id                         " +
        "where                                                                                              " +
        "  i.onec_id rlike :item                                                                            " +
        "  and                                                                                              " +
        "  sto.onec_id   rlike :shop                                                                        ",
    resultClass = RemainsResult.class)
public class RemainsResult implements Serializable {
    @Column(name = "item")
    private String item;
    @Column(name = "item_onec_id")
    private String itemOnecId;
    @Column(name = "item_id")
    private Long itemId;
    @Column(name = "region")
    private String region;
    @Column(name = "region_id")
    private String regionId;
    @Column(name = "delivery_terminal_onec")
    private String deliveryTerminalOnec;
    @Column(name = "pickup_of_terminal")
    private String pickupOfTerminal;
    @Column(name = "stock")
    private String stock;
    @Transient
    private int deliveryDay;
    @Id
    @Column(name = "rule_uid")
    private String ruleUid;
    @Column(name = "time_from")
    private String timeFrom;
    @Column(name = "time_to")
    private String timeTo;
    @Column(name = "sell_terr_formation")
    private String sellTerrFormation;
    @Column(name = "delivery_terminal")
    private String deliveryTerminal;
    @Column(name = "axapta_storage")
    private String axaptaStorage;
    @Column(name = "delivery_days_after")
    private Integer deliveryDaysAfter;
    @Column(name = "this_region")
    private String thisRegion;
    @Column(name = "other_region")
    private String otherRegion;
    @Column(name = "centr_stock")
    private String centrStock;
    @Column(name = "onec_id")
    private String onecId;


    @Override
    public String toString() {
        return "RemainsResult{" +
            "item='" + item + '\'' +
            ", itemOnecId='" + itemOnecId + '\'' +
            ", itemId=" + itemId +
            ", region='" + region + '\'' +
            ", regionId='" + regionId + '\'' +
            ", deliveryTerminalOnec='" + deliveryTerminalOnec + '\'' +
            ", pickupOfTerminal='" + pickupOfTerminal + '\'' +
            ", stock='" + stock + '\'' +
            ", deliveryDay='" + deliveryDay + '\'' +
            ", ruleUid='" + ruleUid + '\'' +
            ", timeFrom='" + timeFrom + '\'' +
            ", timeTo='" + timeTo + '\'' +
            ", sellTerrFormation='" + sellTerrFormation + '\'' +
            ", deliveryTerminal='" + deliveryTerminal + '\'' +
            ", axaptaStorage='" + axaptaStorage + '\'' +
            ", deliveryDaysAfter='" + deliveryDaysAfter + '\'' +
            ", thisRegion='" + thisRegion + '\'' +
            ", otherRegion='" + otherRegion + '\'' +
            ", centrStock='" + centrStock + '\'' +
            ", onecId='" + onecId + '\'' +
            '}';
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItemOnecId() {
        return itemOnecId;
    }

    public void setItemOnecId(String itemOnecId) {
        this.itemOnecId = itemOnecId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getDeliveryTerminalOnec() {
        return deliveryTerminalOnec;
    }

    public void setDeliveryTerminalOnec(String deliveryTerminalOnec) {
        this.deliveryTerminalOnec = deliveryTerminalOnec;
    }

    public String getPickupOfTerminal() {
        return pickupOfTerminal;
    }

    public void setPickupOfTerminal(String pickupOfTerminal) {
        this.pickupOfTerminal = pickupOfTerminal;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public int getDeliveryDay() {
        return deliveryDay;
    }
    @Column(name = "delivery_day")
    public void setDeliveryDay(Integer deliveryDay) {
        if(deliveryDay != null)
            this.deliveryDay = deliveryDay;
    }

    public String getRuleUid() {
        return ruleUid;
    }

    public void setRuleUid(String ruleUid) {
        this.ruleUid = ruleUid;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getSellTerrFormation() {
        return sellTerrFormation;
    }

    public void setSellTerrFormation(String sellTerrFormation) {
        this.sellTerrFormation = sellTerrFormation;
    }

    public String getDeliveryTerminal() {
        return deliveryTerminal;
    }

    public void setDeliveryTerminal(String deliveryTerminal) {
        this.deliveryTerminal = deliveryTerminal;
    }

    public String getAxaptaStorage() {
        return axaptaStorage;
    }

    public void setAxaptaStorage(String axaptaStorage) {
        this.axaptaStorage = axaptaStorage;
    }

    public int getDeliveryDaysAfter() {
        return deliveryDaysAfter;
    }

    public void setDeliveryDaysAfter(Integer deliveryDaysAfter) {

        if(deliveryDaysAfter != null)
            this.deliveryDaysAfter = deliveryDaysAfter;
    }

    public String getThisRegion() {
        return thisRegion;
    }

    public void setThisRegion(String thisRegion) {
        this.thisRegion = thisRegion;
    }

    public String getOtherRegion() {
        return otherRegion;
    }

    public void setOtherRegion(String otherRegion) {
        this.otherRegion = otherRegion;
    }

    public String getCentrStock() {
        return centrStock;
    }

    public void setCentrStock(String centrStock) {
        this.centrStock = centrStock;
    }

    public String getOnecId() {
        return onecId;
    }

    public void setOnecId(String onecId) {
        this.onecId = onecId;
    }
}
