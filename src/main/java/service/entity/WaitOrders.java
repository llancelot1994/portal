package ru.rbt.integration.service.entity;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@NamedNativeQueries({
    @NamedNativeQuery(name = "WaitOrders.getWaitOrders",
        query = "" +
            "select * from web.orders_retry_wait                         " +
            "where date_add>date(now()) and last_response not rlike '#9' ", resultClass = WaitOrders.class)
})
@Table(catalog="web", name = "orders_retry_wait")
public class WaitOrders {
    @Id
    @Column(name="orders_id")
    private long id;
    @Column(name="request")
    private String request;
    @Column(name="payorder_body")
    private String payBody;
    @Column(name="last_response")
    private String response;
    @Column(name="date_add")
    @Temporal(TemporalType.TIME)
    private Date dateTimeAdd;
    @Column(name="waiting")
    private int waiting;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getPayBody() {
        return payBody;
    }

    public void setPayBody(String payBody) {
        this.payBody = payBody;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getDateTimeAdd() {
        return dateTimeAdd;
    }

    @Column(name="date_add")
    public void setDateTimeAdd(Date dateTimeAdd) {
        this.dateTimeAdd = dateTimeAdd;
    }

    public int getWaiting() {
        return waiting;
    }

    public void setWaiting(int waiting) {
        this.waiting = waiting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WaitOrders)) return false;
        WaitOrders that = (WaitOrders) o;
        return getId() == that.getId() &&
            getWaiting() == that.getWaiting() &&
            Objects.equals(getRequest(), that.getRequest()) &&
            Objects.equals(getPayBody(), that.getPayBody()) &&
            Objects.equals(getResponse(), that.getResponse()) &&
            Objects.equals(getDateTimeAdd(), that.getDateTimeAdd());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRequest(), getPayBody(), getResponse(), getDateTimeAdd(), getWaiting());
    }
}
