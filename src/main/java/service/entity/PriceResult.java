package ru.rbt.integration.service.entity;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
@Entity
@NamedNativeQueries({
    @NamedNativeQuery(name ="PriceResult.getPriceLoadResult",
        query ="" +
        "SELECT                                              " +
        "sh.shop_id,                                         " +
        "IFNULL(spu.upload_onec_id, sh.onec_id) shop_onec_id " +
        "    , COUNT(item_upload_id) AS all_prices           " +
        "    , COUNT(                                        " +
        "        IF(price_uploaded = 0, 1, NULL)             " +
        "      ) AS uploading_prices                         " +
        "    , COUNT(                                        " +
        "        IF(price_uploaded = 1, 1, NULL)             " +
        "      ) AS uploaded_prices                          " +
        "    , COUNT(                                        " +
        "        IF(price_uploaded = 2, 1, NULL)             " +
        "      ) AS error_prices                             " +
        "  FROM rbt_integration.item_upload as iu            " +
        "join web.shop sh on iu.shop_id = sh.shop_id         " +
        "    AND (sh.is_retail OR sh.is_main  )              " +
        "                                                    " +
        "JOIN web.region reg_tmp                             " +
        "  ON reg_tmp.region_id = sh.region_id               " +
        "JOIN web.region reg                                 " +
        "  ON reg.status = 1 and reg.region_id = IF(         " +
        "    reg_tmp.price_alias_id != 0                     " +
        "    , reg_tmp.price_alias_id                        " +
        "    , reg_tmp.region_id                             " +
        "  )                                                 " +
        "LEFT JOIN web.shop_price_upload_onec_id spu         " +
        "  ON spu.shop_id = sh.shop_id                       " +
        "  WHERE price > 0                                   " +
        "GROUP BY sh.shop_id                                 " +
        "ORDER BY sh.onec_id                                 ", resultClass = PriceResult.class),
    @NamedNativeQuery(name ="PriceResult.getPriceLoadResultFromMask",
        query ="" +
            "SELECT                                              " +
            "sh.shop_id,                                         " +
            "IFNULL(spu.upload_onec_id, sh.onec_id) shop_onec_id " +
            "    , COUNT(item_upload_id) AS all_prices           " +
            "    , COUNT(                                        " +
            "        IF(price_uploaded = 0, 1, NULL)             " +
            "      ) AS uploading_prices                         " +
            "    , COUNT(                                        " +
            "        IF(price_uploaded = 1, 1, NULL)             " +
            "      ) AS uploaded_prices                          " +
            "    , COUNT(                                        " +
            "        IF(price_uploaded = 2, 1, NULL)             " +
            "      ) AS error_prices                             " +
            "  FROM rbt_integration.item_upload as iu            " +
            "join web.shop sh on iu.shop_id = sh.shop_id         " +
            "    AND (sh.is_retail OR sh.is_main  )              " +
            "                                                    " +
            "JOIN web.region reg_tmp                             " +
            "  ON reg_tmp.region_id = sh.region_id               " +
            "JOIN web.region reg                                 " +
            "  ON reg.status = 1 and reg.region_id = IF(         " +
            "    reg_tmp.price_alias_id != 0                     " +
            "    , reg_tmp.price_alias_id                        " +
            "    , reg_tmp.region_id                             " +
            "  )                                                 " +
            "LEFT JOIN web.shop_price_upload_onec_id spu         " +
            "  ON spu.shop_id = sh.shop_id                       " +
            "  WHERE price > 0                    and            " +
            "IFNULL(spu.upload_onec_id, sh.onec_id) rlike ?      " +
            "GROUP BY sh.shop_id                                 " +
            "ORDER BY sh.onec_id                                 ", resultClass = PriceResult.class),
    @NamedNativeQuery(name ="PriceResult.reloadOnRemain",
        query ="" +
        "insert into rbt_integration.item_upload                                            " +
        "(item_upload_id, item_id, shop_id, price, old_price, actual_date, price_uploaded)  " +
        " select 0, item_id, shop_id, p.price, p.old_price, p.actual_date, 0                " +
        " from web.item_remain as r                                                         " +
        "left join rbt_integration.item_upload as iu using(item_id, shop_id)                " +
        "join web.item as i using(item_id)                                                  " +
        "join web.regions_shops as rs using(shop_id)                                        " +
        "join accounting.item_price_rbt as p using(region_id, item_id)                      " +
        "where shop_id = ? and iu.item_id is null                                           " , resultClass = CountModified.class),
    @NamedNativeQuery(name ="PriceResult.reload",
        query ="" +
            "UPDATE rbt_integration.item_upload iu " +
            "SET iu.price_uploaded = 0             " +
            "WHERE iu.shop_id = ?                  ", resultClass = CountModified.class),
    @NamedNativeQuery(name ="PriceResult.reloadOnError",
        query ="" +
            "UPDATE rbt_integration.item_upload iu " +
            "SET iu.price_uploaded = 0             " +
            "WHERE iu.shop_id = ?                  " +
            "AND iu.price_uploaded =2              ", resultClass = CountModified.class)
})
public class PriceResult implements Serializable {
    @Column(name="shop_id")
    private int id;
    @Column(name="shop_onec_id") @Id
    private String shopId;
    @Column(name="all_prices")
    private int allCount;
    @Column(name="uploading_prices")
    private int uploadingPrices;
    @Column(name="uploaded_prices")
    private int uploadedPrices;
    @Column(name="error_prices")
    private int errorPrices;

    public String getShopId() {
        return shopId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public int getAllCount() {
        return allCount;
    }

    public void setAllCount(int allCount) {
        this.allCount = allCount;
    }

    public int getUploadingPrices() {
        return uploadingPrices;
    }

    public void setUploadingPrices(int uploadingPrices) {
        this.uploadingPrices = uploadingPrices;
    }

    public int getUploadedPrices() {
        return uploadedPrices;
    }

    public void setUploadedPrices(int uploadedPrices) {
        this.uploadedPrices = uploadedPrices;
    }

    public int getErrorPrices() {
        return errorPrices;
    }

    public void setErrorPrices(int errorPrices) {
        this.errorPrices = errorPrices;
    }
}
