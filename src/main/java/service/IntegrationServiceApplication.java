package ru.rbt.integration.service;

import org.apache.commons.cli.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@SpringBootApplication
public class IntegrationServiceApplication {

	public static void main(String[] args) {
	    printLogo();
        Options options = new Options();

        Option help = new Option("h", "help", false, "show help");
        help.setRequired(false);
        options.addOption(help);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("help")){
                helpShow();
                return;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            formatter.printHelp("service", options);
        }
		ConfigurableApplicationContext context= SpringApplication.run(IntegrationServiceApplication.class, args);

	}

    private static ApplicationContext context;


    private static void helpShow(){
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(IntegrationServiceApplication.class.getResource("/help").openStream()));
            String line;
            while ((line = reader.readLine()) != null)
            {
                System.out.println(line);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void printLogo(){
        System.out.println("" +
                "                                                                                 ooddddddoo       \n"+
                "                                                                         oxxxdddddddddddddxxkxo   \n"+
                "      dxxxxxxxxddo        dxxxxxxxxdd       dxxxxxxxxxxxxxxxxd          dkd               c okOd  \n"+
                "     xXWWWWWWWWWNX0x     xXWWWWWWWWWNXOd   kNWWWWWWWWWWWWWWWWO         dxd                    xOdc\n"+
                "    o0WMWX0OOOKNMMWXx    OWMMXOkOOKWMMW0oc xO0000KNMMMN000O0Od       oddd    c cc             okxc\n"+
                "   cdXMMWO c c 0WMMWk   dXWWWk   coKWMWKdc     cdOXMMWO             dxoodoodddxxxkkkkxxddo    okdc\n"+
                "    kWMMXxc   dKWMWKdc  kWMWXdc  okNWWXx        kNWMMNx             odddddddoooddooodddddo  c xko \n"+
                "   oKWMMXkxkk0XWMWKd   oKMMMN0000XWWXOd       coKWWMWKoc           d00kxOKKk dOK0d   oOK0x  dkx   \n"+
                "   xNWMMWWWWWMWXOxo    xNMMWWNNWWWMWN0d        xXWMMWk             OWWNNXKKx OWWXd   kNWNx xkd    \n"+
                "   0WMMXOkOXWMWXx      OWMMKxddddkXMMWXdc      OWMMWXd           cdKWWNko   oKWW0o  o0WWKo kxc    \n"+
                " cdXWMWO ccdKWMWXd   cdXWMWk      0WMMNxc    cdXWMMWO             kNMNk     xNWNx   xXWNk  xx     \n"+
                "  OWMMXx    kNMMWO    kWMWXdc   okNMMW0o      kNMMMNxc     ooo  co0WWKo    o0WWKdc dKWWXdc dd     \n"+
                "coKWMW0o   cdXWMWKo  oKWMMNK000KXWWWXOo      o0WMMWKo   coOXX0d cxXWNk    coKWMNOk0XWMW0o odo     \n"+
                " dKNNXx     oONNNKd cdKNNNNNNNNNXKOko       cdKNNNXk    cdKNN0oc kNNKdc     xKNWWNKOKNXx  o       \n"+
                "  dddo       odddo    oddddddddoo             ddddo       oddo   oddo        odxxd  odo           \n"+
                "                                                                                                  \n");
    }
}
