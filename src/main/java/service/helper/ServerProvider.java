package ru.rbt.integration.service.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ServerProvider {
    @Value("${cacheCleaner.user}")
    private String username;
    @Value("${cacheCleaner.key}")
    private String keyPath;
    @Value("${cacheCleaner.hosts}")
    private String knowHosts;
    public Map<String, Server> getServers() {
        if(servers == null)
            create();
        return servers;
    }
    private Map<String, Server> servers = null;
    public void create(){
        servers = new HashMap();
        servers.put("pegas6" , new Server(username, "10.175.1.33",   3322 , keyPath, knowHosts));
        servers.put("pegas1" , new Server(username, "185.78.29.95",  3322 , keyPath, knowHosts));
        servers.put("pegas2" , new Server(username, "185.78.29.97",  3322 , keyPath, knowHosts));
        servers.put("pegas3" , new Server(username, "185.78.29.99",  3322 , keyPath, knowHosts));
        servers.put("pegas4" , new Server(username, "185.78.29.101", 3322 , keyPath, knowHosts));
        servers.put("pegas5" , new Server(username, "185.78.29.103", 3322 , keyPath, knowHosts));
        servers.put("rbtech3", new Server(username, "185.78.29.82",  22   , keyPath, knowHosts));
    }
}
