package ru.rbt.integration.service.helper;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.HashMap;

@Component
public class SystemHelper {
    private static Logger logger = LoggerFactory.getLogger(SystemHelper.class);
    @Autowired
    private ServerProvider serversPrpovider;
    @Autowired
    private Session session = null;

    public String run(String command){
        synchronized (this) {
            StringBuilder outputBuffer = new StringBuilder();
            try {
                if (!session.isConnected()) {
                    session.connect();
                }
                Channel channel = session.openChannel("exec");
                ((ChannelExec) channel).setCommand(command);
                InputStream commandOutput = channel.getInputStream();
                channel.connect();
                int readByte = commandOutput.read();
                while (readByte != 0xffffffff) {
                    outputBuffer.append((char) readByte);
                    readByte = commandOutput.read();
                }

                channel.disconnect();
            } catch (IOException ioX) {
                logger.error("Не удалось запустить комманду",ioX);
                return null;
            } catch (JSchException jschX) {
                logger.error("Не удалось запустить комманду",jschX);
                return null;
            } finally {
            }
            return outputBuffer.toString();
        }
    }

    public HashMap<String, String> runOnSiteNodes(Command command, String posfix) {
        return runOnSiteNodes(command.toString(posfix));
    }
    public HashMap<String, String> runOnSiteNodes(String command){
        synchronized (this) {
            HashMap<String, String> serverToResultMap = new HashMap<String, String>();
            serversPrpovider.getServers().keySet().forEach(
                server -> {
                    SystemConnector connector = null;
                    try {
                        connector = new SystemConnector(serversPrpovider.getServers().get(server));
                    } catch (JSchException e) {
                        e.printStackTrace();
                    }
                    String result = connector.run( command  );
                    if(result == null){
                        logger.error("Не удалось запустить комманду");
                    } else {
                        logger.info("На сервере " + server + " запущенна комманда '" + command + "' с результатом " + result);
                    }
                    serverToResultMap.put(server, result);
                }
            );
            return serverToResultMap;
        }
    }
}
