package ru.rbt.integration.service.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

public class Server {
    private String username;
    private String host;
    private Integer port;
    private String keyPath;
    private String knowHost;

    public Server(String username, String host, Integer port) {
        this.username = username;
        this.host = host;
        this.port = port;
    }
    public Server(String username, String host, Integer port, String keyPath) {
        this(username,  host,  port);
        this.keyPath = keyPath;
    }
    public Server(String username, String host, Integer port, String keyPath, String knowHost) {
        this(username,  host,  port, keyPath);
        this.knowHost = knowHost;
    }
    public String getUsername() {
        return username;
    }

    public String getHost() {
        return host;
    }

    public Integer getPort() {
        return port;
    }

    public String getKeyPath() {
        return keyPath;
    }

    public String getKnowHost() {
        return knowHost;
    }
}
