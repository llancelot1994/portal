package ru.rbt.integration.service.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Command {
    @Value("${cacheCleaner.command}")
    private String command = "";
    private Map<String, Integer> postfix = new HashMap<String, Integer>();
    @Autowired
    private SystemHelper systemHelper;
    public Command(String command, Map<String, Integer> postfix) {
        this.command = command;
        this.postfix = postfix;
    }
    public Command(){
//        command = "/usr/share/workflow-scripts/prod/memcached.sh "; //очистка кеша

        postfix.put("Открытие нового салона", 1);
        postfix.put("Новая информация по регионам, адресам, времени работы", 2);
        postfix.put("Баннеры акций", 3);
        postfix.put("Условия рекламных акций, плашки", 4);
        postfix.put("Топ 20", 5);
        postfix.put("Превью контента в категориях", 6);
        postfix.put("Меню категорий", 7);
        postfix.put("Side-by-side", 8);
        postfix.put("Мета каталога", 9);
        postfix.put("Сортировка брендов в фильтрах", 10);
        postfix.put("Контентные страницы", 11);
        postfix.put("Гарантия (ГОСТ)", 12);
        postfix.put("Список онлайн оплат в регионах", 13);
        postfix.put("Начисление бонусов в товарах", 14);
        postfix.put("Структура меню", 15);
        postfix.put("Сроки доставки/самовывоза", 16);
        postfix.put("Данные посадочных страниц", 17);
        postfix.put("Информация об алиасах и описания каталогов", 18);
        postfix.put("Фильтры", 19);
    }

    public String getCommands() {
        return command;
    }

    public Map<String, Integer> getPostfix() {
        return postfix;
    }

    public String toString(String postfix){
        return command +" "+ postfix;
    }

    public HashMap<String, String> run(String postfix) {
        return systemHelper.runOnSiteNodes(this, postfix);
    }
}
