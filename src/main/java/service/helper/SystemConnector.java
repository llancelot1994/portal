package ru.rbt.integration.service.helper;

import com.jcraft.jsch.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

//@Slf4j
public class SystemConnector {
    private static String keyPath = "~/.ssh/key";
    private static String knownHostsPath = "~/.ssh/known_hosts";

    public SystemConnector(Session session) {
        this.session = session;
    }

    public SystemConnector(Server server) throws JSchException {
        JSch jsch = new JSch();
        jsch.addIdentity(server.getKeyPath());
        jsch.setKnownHosts(server.getKnowHost());
        session = jsch.getSession(server.getUsername(), server.getHost(), server.getPort());
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
    }

    private Session session = null;

    public String run(String command) {
        synchronized (session.getHost().intern()) {
            ArrayList<Byte> outputBuffer = new ArrayList<Byte>();
            try {
                if (!session.isConnected()) {
                    session.connect();
                }
                Channel channel = session.openChannel("exec");
                ((ChannelExec) channel).setCommand(command);
                InputStream commandOutput = channel.getInputStream();
                channel.connect();
                int readByte = commandOutput.read();
                while (readByte != 0xffffffff) {
                    outputBuffer.add((byte) readByte);
                    readByte = commandOutput.read();
                }

                channel.disconnect();
            } catch (IOException ioX) {
                return null;
            } catch (JSchException e) {
                e.printStackTrace();
            } finally {
            }
            byte[] bytes = new byte[outputBuffer.size()];
            for (int i = 0; i < outputBuffer.size(); i++) {
                bytes[i] = outputBuffer.get(i);
            }
            return new String(bytes);
        }
    }
}
