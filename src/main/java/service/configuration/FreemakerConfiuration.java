package ru.rbt.integration.service.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import java.io.File;

@Configuration
public class FreemakerConfiuration  {
  @Bean
  public FreeMarkerConfigurer freeMarkerConfigurer(WebApplicationContext applicationContext) throws Exception {
    FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
    if(new File("/app/templates").exists())
      configurer.setTemplateLoaderPath("file:/app/templates");
    else
      configurer.setTemplateLoaderPath("classpath:/templates");
    return configurer;
  }
  @Bean
  public FreeMarkerViewResolver freemarkerViewResolver() {
    FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
    resolver.setCache(true);
    resolver.setPrefix("/freemarker/");
    resolver.setSuffix(".ftl");
    resolver.setOrder(1);
    return resolver;
  }
}
