package ru.rbt.integration.service.configuration;


import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileUrlResource;

import java.io.File;

@Configuration
@ComponentScan(basePackages = { "ru.rbt.integration.service" })
@Import({ OrmConfig.class })
public class ApplicationConfiguration implements DisposableBean
{
  @Value("${ssh.host}")
  private String host = "185.78.29.105";
  @Value("${ssh.port}")
  private int port = 3322;
  @Value("${ssh.username}")
  private String username = "rbt-integration";
  @Value("${ssh.key.path}")
  private String keyPath = "~/.ssh/key";
  @Value("${ssh.known.hosts.path}")
  private String knownHostsPath = "~/.ssh/known_hosts";

  private Session currentSession;

  @Bean
  public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
    PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
    try {
      if(new File("/var/local/application.properties").exists()) {
        ppc.setLocation(new FileUrlResource("/var/local/application.properties"));
      }
      else{
        ppc.setLocation(new ClassPathResource("application.properties"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    ppc.setFileEncoding("UTF-8");
    ppc.setIgnoreUnresolvablePlaceholders(true);
    return ppc;
  }


  @Bean
  public Session SshSession() throws JSchException {
    JSch jsch = new JSch();
    jsch.addIdentity(keyPath);
    jsch.setKnownHosts(knownHostsPath);
    currentSession = jsch.getSession(username, host, port);
    java.util.Properties config = new java.util.Properties();
    config.put("StrictHostKeyChecking", "no");
    currentSession.setConfig(config);
    return currentSession;
  }

  @Override
  public void destroy() throws Exception {
    if (currentSession.isConnected()) {
      currentSession.disconnect();
    }
  }
}