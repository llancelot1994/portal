package ru.rbt.integration.service.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;


@Configuration
@EnableConfigurationProperties
@EnableTransactionManagement
@ComponentScan
@EnableJpaRepositories(basePackages = {"ru.rbt.integration.service"})
public class OrmConfig {
  // Свойства источника данных
  @Value("${spring.datasource.driver-class-name}")
  private String driverClassName;
  @Value("${spring.datasource.url}")
  private String url;
  @Value("${spring.datasource.username}")
  private String username;
  @Value("${spring.datasource.password}")
  private String password;
  @Value("${spring.jpa.hibernate.implicit_naming_strategy}")
  private Object implicitNamingStrategy;

  /**
   * Компонент источника данных
   */
  @Bean
  @ConfigurationProperties("app.datasource")
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(driverClassName);
    dataSource.setUrl(url);
    dataSource.setUsername(username);
    dataSource.setPassword(password);
    return dataSource;
  }

  // Свойства Hibernate
  @Value("${spring.jpa.properties.hibernate.dialect}")
  private String hibernateDialect;
  @Value("${spring.jpa.show-sql}")
  private String hibernateShowSql;
  @Value("${spring.jpa.hibernate.ddl-auto}")
  private String hibernateHBM2DDLAuto;
  @Value("${spring.jpa.properties.hibernate.current_session_context_class}")
  private String hibernateContextClass;

  /**
   * Свойства Hibernate в виде объекта класса Properties
   */
  public Properties hibernateProperties() {
    Properties properties = new Properties();
    properties.put("hibernate.dialect", hibernateDialect);
    properties.put("hibernate.show_sql", hibernateShowSql);
    properties.put("hibernate.hbm2ddl.auto", hibernateHBM2DDLAuto);
    properties.put("hibernate.current_session_context_class",hibernateContextClass);
    properties.put("hibernate.implicit_naming_strategy",implicitNamingStrategy);
    properties.put("hibernate.connection.charSet","UTF-8");
    properties.put("hibernate.connection.CharacterEncoding","UTF-8");
    properties.put("hibernate.connection.Useunicode","true");
    properties.put("hibernate.dialect", hibernateDialect);
//    properties.put("provider_class","ru.rbt.integration.connectionToResource.configuration.connection.HibernateConnectionProvider");
    properties.put("cache.provider_class","org.hibernate.cache.NoCacheProvider");
    properties.put("hibernate.connection.handling_mode","DELAYED_ACQUISITION_AND_RELEASE_AFTER_TRANSACTION");
    properties.put("hibernate.connection.pool_size","2");
    properties.put("hbm2ddl.auto","none");
    properties.put("hibernate.connection.url",url);
    properties.put("hibernate.connection.username", username);
    properties.put("hibernate.connection.password", password);
    properties.put("hibernate.cache.provider_class","org.hibernate.cache.NoCacheProvider");
    properties.put("hibernate.cache.use_second_level_cache","false");
    return properties;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
    lef.setDataSource(dataSource());
    lef.setJpaVendorAdapter(jpaVendorAdapter());
    lef.setJpaDialect(jpaDialect());
    lef.setMappingResources();
    Properties props = hibernateProperties();
    lef.setJpaProperties(props);
    lef.afterPropertiesSet();
    return lef;
  }

  @Autowired
  @Bean
  public JpaVendorAdapter jpaVendorAdapter() {
    HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
    hibernateJpaVendorAdapter.setShowSql(true);
    hibernateJpaVendorAdapter.setGenerateDdl(false);
    hibernateJpaVendorAdapter.setDatabasePlatform(hibernateDialect);

    return hibernateJpaVendorAdapter;
  }

  @Bean
  public JpaDialect jpaDialect() {
    JpaDialect jpaDialect = new HibernateJpaDialect();
    return jpaDialect;
  }

}